﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Engagency.FizzleBizzle.Startup))]
namespace Engagency.FizzleBizzle
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
